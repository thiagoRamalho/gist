package br.com.trama.gists;

/**
 * Created by trama on 15/03/16.
 */
public interface Const {

    interface Pattern {
        String DB_FORMAT = "yyyy-MM-dd HH:mm:ss";
        String DB_FORMAT_TIMEZONE = DB_FORMAT.concat(" Z");
    }
}
