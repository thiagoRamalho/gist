package br.com.trama.gists;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by trama on 15/03/16.
 */
public class FormatDateUtils {

    private static final String TAG = FormatDateUtils.class.getSimpleName();


    public Date strToDate(String strDateValue, String pattern, TimeZone... tz){

        Date date = null;

        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            TimeZone timeZone = TimeZone.getDefault();

            if(tz != null && tz.length > 0){
                timeZone = tz[0];
            }

           sdf.setTimeZone(timeZone);

            date = sdf.parse(strDateValue);

        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }

        return date;
    }

    public String format(Date date, String pattern, TimeZone... tz){

        String strDate = "";

        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            TimeZone timeZone = TimeZone.getDefault();

            if(tz != null && tz.length > 0){
                timeZone = tz[0];
            }

            sdf.setTimeZone(timeZone);

            strDate = sdf.format(date);

        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }

        return strDate;
    }
}
