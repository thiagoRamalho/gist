package br.com.trama.gists;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText viewById;
    private TextView error;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewById = (EditText) findViewById(R.id.edittext_id);
        error = (TextView) findViewById(R.id.error_id);
        this.error.setText(getString(R.string.app_name));
    }
}
