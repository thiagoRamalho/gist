package br.com.trama.gists;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.TimeZone;

import static org.junit.Assert.assertEquals;

/**
 * Created by trama on 15/03/16.
 */
public class FormatDateUtilsTest {

    private static final String DATE_UTC = "2016-03-02 23:59:59 +0000";
    private static final String DATE_AMERICA_SAO_PAULO = "2016-03-02 20:59:59 -0300";
    private static final String TIMEZONE_AMERICA_SAO_PAULO = "America/Sao_Paulo";
    private static final String TIMEZONE_UTC = "UTC";
    private FormatDateUtils fdu;

    @Before
    public void setUp(){
        fdu = new FormatDateUtils();
    }

    @Test
    public void shouldBeConvertWithoutTimezone(){

        TimeZone timeZone = TimeZone.getTimeZone(TIMEZONE_UTC);

        Date strToDate = fdu.strToDate(DATE_UTC, Const.Pattern.DB_FORMAT, timeZone);

        String format = fdu.format(strToDate, Const.Pattern.DB_FORMAT_TIMEZONE, timeZone);

        assertEquals(DATE_UTC, format);
    }

    @Test
    public void shouldBeConvertWithTimezone(){

        TimeZone timeZone = TimeZone.getTimeZone(TIMEZONE_UTC);

        Date strToDate = fdu.strToDate(DATE_UTC, Const.Pattern.DB_FORMAT, timeZone);

        timeZone = TimeZone.getTimeZone(TIMEZONE_AMERICA_SAO_PAULO);

        String format = fdu.format(strToDate, Const.Pattern.DB_FORMAT_TIMEZONE, timeZone);

        assertEquals(DATE_AMERICA_SAO_PAULO, format);
    }

}
