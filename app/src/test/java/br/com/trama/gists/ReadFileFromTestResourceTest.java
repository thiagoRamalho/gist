package br.com.trama.gists;

import org.junit.Test;

import br.com.trama.gists.utilities.TestFileUtil;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by trama on 15/03/16.
 */
public class ReadFileFromTestResourceTest extends TestFileUtil{

    @Test
    public void shouldBeReadFileFromRes(){
        String content = getStrFromResourceFile(this, "test.txt");
        assertEquals("text", content);
    }
}
