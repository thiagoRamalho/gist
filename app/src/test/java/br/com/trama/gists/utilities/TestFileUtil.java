package br.com.trama.gists.utilities;

import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by trama on 15/03/16.
 */
public class TestFileUtil {


    public String getStrFromResourceFile(Object obj, String filename){
        String content = "";
        try {
            InputStream resourceStream = getResourceStream(filename);
            content = convertStreamToString(resourceStream);
        } catch (Exception e) {
            Log.d(this.getClass().getSimpleName(), e.getMessage(), e);
        }

        return content;
    }

    public String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();
        return sb.toString();
    }

    private InputStream getResourceStream(String fileName) {
        ClassLoader classLoader = this.getClass().getClassLoader();
        return classLoader.getResourceAsStream(fileName);
    }
}
